var fs = require('fs');
var assetsMap = JSON.parse(fs.readFileSync('build/asset-manifest.json', 'utf8'));

var manifest = fs.readFileSync('build/manifest.json', 'utf8');

manifest = manifest.split('main.js').join(assetsMap['main.js']);
manifest = manifest.split('main.css').join(assetsMap['main.css']);

fs.writeFile("build/manifest.json", manifest, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});