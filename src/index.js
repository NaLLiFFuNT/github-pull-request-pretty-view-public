import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/App/App';
import { store } from './createStore';
import { CheckUrlAction } from './actions/plugin.actions';

// Init url Binding
store.dispatch(CheckUrlAction(true));
setInterval(() => {
  store.dispatch(CheckUrlAction());
}, 1000);

// Mount plugin
const rootDiv = document.createElement('div');
rootDiv.id = 'github-pull-request-pretty-view-root';
document.body.appendChild(rootDiv);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootDiv
);
