import { applyMiddleware, combineReducers, createStore } from 'redux';
import { pluginReducer } from './reducers/plugin';
import thunk from 'redux-thunk';
import { fileTreeReducer } from './reducers/file-tree';
import { viewedChangesReducer } from './reducers/viewed-changes';

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
export const store = createStore(
  combineReducers({
    plugin: pluginReducer,
    fileTree: fileTreeReducer,
    viewedChanges: viewedChangesReducer
  }),
  applyMiddleware(thunk)
);
