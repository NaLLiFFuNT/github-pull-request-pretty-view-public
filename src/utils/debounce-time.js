export const debounceTimeCreator = time => {
  let timer;
  return method => {
    if (timer) {
      clearTimeout(timer);
    }
    setTimeout(method, time);
  };
};
