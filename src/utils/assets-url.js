const chrome = window.chrome;
export const assetsUrl = (v) => chrome && chrome.extension ? chrome.extension.getURL(v) : v;
