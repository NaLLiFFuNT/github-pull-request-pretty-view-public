export function HashRot13(str) {
  var hash = 0;

  for (var i = 0; i < str.length; i++) {
    hash += str.charCodeAt(i);
    hash -= (hash << 13) | (hash >> 19);
  }

  return hash.toString(36) + '-' + str.length.toString(36);
}
