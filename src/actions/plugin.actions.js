import {
  HIDE_PLUGIN,
  SHOW_PLUGIN,
  UPDATE_PAGE_URL
} from '../constants/plugin.action-types';
import {
  initTreeForPullRequest,
  stopBindTreeFromPage
} from './file-tree.actions';

const targetUrlRegExp = /^https?:\/\/(github.com|localhost:3000)\/.*\/pull\/([\d]+)\/files($|#(.*)$)/i;

export const updatePageUrl = newUrl => {
  return {
    type: UPDATE_PAGE_URL,
    payload: newUrl
  };
};

const showPlugin = () => {
  return {
    type: SHOW_PLUGIN
  };
};

export const hidePlugin = () => (dispatch, getState) => {
  dispatch(stopBindTreeFromPage());
  dispatch({
    type: HIDE_PLUGIN
  });
};

export const CheckUrlAction = (initial = false) => (dispatch, getState) => {
  if (getState().plugin.url !== window.location.href || initial) {
    dispatch(updatePageUrl(window.location.href));

    // location's changed => check what to do
    if (targetUrlRegExp.test(getState().plugin.url)) {
      if (!getState().plugin.enabled) {
        dispatch(showPlugin());
      }

      const newPull = targetUrlRegExp.exec(getState().plugin.url)[2];
      const changesId = 'pull-request-' + newPull;

      if (getState().plugin.changesId !== changesId) {
        return dispatch(initTreeForPullRequest(changesId));
      }
      return;
    }

    return dispatch(hidePlugin());
  }
};
