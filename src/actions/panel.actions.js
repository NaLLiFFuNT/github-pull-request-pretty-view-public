import { HIDE_PANEL, SHOW_PANEL } from '../constants/panel.action-types';
import { stopBindTreeFromPage } from './file-tree.actions';

export const showPanel = () => {
  return {
    type: SHOW_PANEL
  };
};

export const hidePanel = () => (dispatch, getState) => {
  dispatch(stopBindTreeFromPage());
  dispatch({
    type: HIDE_PANEL
  });
};
