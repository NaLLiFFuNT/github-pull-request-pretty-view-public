import { UPDATE_FILE_TREE } from '../constants/file-tree.action-types';
import { HashRot13 } from '../utils/hash';
import { debounceTimeCreator } from '../utils/debounce-time';

let activeSubscription;

const observerConfig = { childList: true, characterData: true, subtree: true };
let observer;

function addIntoTree(item, currentNode) {
  let path = item.path[0];

  if (item.path.length === 1) {
    currentNode.children.push({
      path: path,
      commit: item.commit,
      linkHashToView: item.linkHashToView,
      changesHash: item.changesHash
    });
    return currentNode;
  }

  let pushItem = function(node) {
    addIntoTree(
      {
        path: item.path.slice(1),
        commit: item.commit,
        linkHashToView: item.linkHashToView,
        changesHash: item.changesHash
      },
      node
    );
  };

  // try to find node for path
  if (
    !currentNode.children.some(function(node) {
      if (node.path === path) {
        pushItem(node);
        return true;
      }
      return false;
    })
  ) {
    // new node
    let node = {
      path: path,
      children: []
    };
    currentNode.children.push(node);
    pushItem(node);
  }
  return currentNode;
}
// join folders who contains one folder
function optimizeTree(node) {
  if (!node.children) {
    return node;
  }
  // join phponly folders
  if (node.children.length === 1 && !node.children[0].commit) {
    let child = node.children[0];
    child.path = [node.path, child.path].join('/');
    return optimizeTree(child);
  }
  node.children = node.children.map(optimizeTree);
  return node;
}

function buildTreeByContent() {
  const list = Array.from(document.querySelectorAll('div.file-header'))
    .map(function(item) {
      if (!item.getAttribute('data-path')) {
        return null;
      }
      let anchor = item.getAttribute('data-anchor');
      let contentContainer = Array.from(
        item.parentNode.querySelectorAll('.js-file-content tr')
      )
        .filter(i => !i.className)
        .map(i => i.innerText)
        .join();

      let changesHash = anchor + HashRot13(contentContainer);

      return {
        //commitHash: commitHash,
        path: item.getAttribute('data-path')
          ? item.getAttribute('data-path').split('/')
          : '',
        commit: anchor,
        linkHashToView: anchor,
        changesHash: changesHash
      };
    })
    .filter(x => x);
  let tree = {
    path: '',
    children: []
  };
  list.forEach(function(item) {
    addIntoTree(item, tree);
  });

  return optimizeTree(tree);
}

export const initTreeForPullRequest = changesId => (dispatch, getState) => {
  dispatch(bindTreeFromPage());
  //  console.log('initTreeForPullRequest');
  return {
    type: 'initTreeForPullRequest',
    payload: changesId
  };
};

const updateFileTree = tree => {
  return {
    type: UPDATE_FILE_TREE,
    payload: tree
  };
};

const debounceTime = debounceTimeCreator(400);

export const bindTreeFromPage = () => (dispatch, getState) => {
  if (activeSubscription) {
    dispatch(stopBindTreeFromPage());
  }

  const observerTarget = document.querySelector('div[role="main"]');
  if (!observerTarget) {
    //console.log(INIT_MESSAGE_FAILED);
    return false;
  }

  const grabTree = () =>
    debounceTime(() => {
      // grab tree
      dispatch(updateFileTree(buildTreeByContent()));
    });

  observer = new MutationObserver(function(mutations) {
    mutations.forEach(grabTree);
  });

  observer.observe(observerTarget, observerConfig);
  grabTree();
};

export const stopBindTreeFromPage = () => (dispatch, getState) => {
  if (observer) {
    observer.disconnect();
  }
};
