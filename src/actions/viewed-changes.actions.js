import { MARK_AS_VIEWED } from '../constants/viewed-changes.action-types';

const LOCAL_STORAGE_KEY = 'GitHub pull request pretty Viewer';

export function getStoredVisitedValues() {
  let result;
  try {
    result = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
  } catch (e) {}
  // if it's plain object
  if (result && typeof result === 'object' && !Array.isArray(result)) {
    return result;
  }
  return {};
}

function storeVisitedValues(newValues) {
  let visitedCache = { ...getStoredVisitedValues(), ...newValues };
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(visitedCache));
}

export const markAsViewed = hash => (dispatch, getState) => {
  dispatch({
    type: MARK_AS_VIEWED,
    payload: hash
  });
  storeVisitedValues(getState().viewedChanges);
};
