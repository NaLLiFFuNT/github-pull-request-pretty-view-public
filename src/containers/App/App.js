import React, { Component } from 'react';
import PropTypes from 'prop-types'; // ES6

import { connect } from 'react-redux';
import PluginIcon from '../../components/plugin-icon/PluginIcon';
import { hidePanel, showPanel } from '../../actions/panel.actions';
import Panel from '../../components/panel/Panel';

class App extends Component {
  getChildContext() {
    return { dispatch: this.props.dispatch };
  }
  showPanel = () => {
    this.props.dispatch(showPanel());
  };
  hidePanel = () => {
    this.props.dispatch(hidePanel());
  };
  render() {
    const propsStr = JSON.stringify(this.props);
    if (this.props.plugin.enabled && !this.props.plugin.expanded) {
      return (
        <div onClick={this.showPanel}>
          <PluginIcon />
        </div>
      );
    }

    if (this.props.plugin.enabled && this.props.plugin.expanded) {
      return (
        <Panel
          onClose={this.hidePanel}
          fileTree={this.props.fileTree}
          viewedChanges={this.props.viewedChanges}
        />
      );
    }
    return <div/>;
  }
}

App.childContextTypes = {
  dispatch: PropTypes.func
};

const mapStateToProps = state => state;
export default connect(mapStateToProps)(App);
