import './Panel.css';
import React, { Component } from 'react';
import TreeItem from '../tree-item/TreeItem';

class Panel extends Component {
  componentWillMount() {
    document.body.classList.add('pretty_view_pull_request_body_decorating');
  }
  componentWillUnmount() {
    document.body.classList.remove('pretty_view_pull_request_body_decorating');
  }
  render() {
    return (
      <div className="Panel">
        <div className="CloseCross" onClick={this.props.onClose}>
          <svg
            aria-hidden="true"
            className="pretty_view_pull_request_close_cross"
            height="16"
            version="1.1"
            viewBox="0 0 12 16"
            width="12"
            style={{ transform: 'rotate(45deg)' }}
          >
            <path fillRule="evenodd" d="M12 9H7v5H5V9H0V7h5V2h2v5h5z" />
          </svg>
        </div>
        <div className="PanelTreeContent">
          <TreeItem
            item={this.props.fileTree}
            viewedChanges={this.props.viewedChanges}
          />
        </div>
      </div>
    );
  }
}
export default Panel;
