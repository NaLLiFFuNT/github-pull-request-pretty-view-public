import './PluginIcon.css';
import React, { Component } from 'react';
import { assetsUrl } from '../../utils/assets-url';

import treeIcon from './tree-icon.png';

class PluginIcon extends Component {
  render() {
    return (
      <div className="PluginIcon">
        <img src={assetsUrl(treeIcon)} alt="FileTree" />
      </div>
    );
  }
}
export default PluginIcon;
