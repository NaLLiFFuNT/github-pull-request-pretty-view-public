import React, { Component } from 'react';
import './TreeItem.css';
import { markAsViewed } from '../../actions/viewed-changes.actions';
import PropTypes from 'prop-types';

class TreeItem extends Component {
  markAsVisited = () => {
    this.context.dispatch(markAsViewed(this.props.item.changesHash));
  };
  render() {
    const item = this.props.item;

    if (item.commit) {
      return (
        <div className="TreeItem">
          <a
            href={'#' + item.linkHashToView}
            className={
              this.props.viewedChanges[item.changesHash] ? 'visited' : ''
            }
            onClick={this.markAsVisited}
          >
            {item.path}
          </a>
        </div>
      );
    }
    return (
      <div className="TreeItem">
        <div className="TreeItem-folder">{item.path}</div>
        <div className="TreeItem-children">
          {item.children.map((child, index) => (
            <TreeItem
              key={index}
              item={child}
              viewedChanges={this.props.viewedChanges}
            />
          ))}
        </div>
      </div>
    );
  }
}

TreeItem.contextTypes = {
  dispatch: PropTypes.func
};

export default TreeItem;
