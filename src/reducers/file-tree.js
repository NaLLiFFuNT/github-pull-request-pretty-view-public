import { UPDATE_FILE_TREE } from '../constants/file-tree.action-types';

const initialState = {
  path: '',
  children: []
};

export const fileTreeReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_FILE_TREE: {
      return { ...action.payload };
    }
    default:
      return state;
  }
};
