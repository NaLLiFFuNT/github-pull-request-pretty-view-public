import {
  HIDE_PLUGIN,
  SHOW_PLUGIN,
  UPDATE_PAGE_URL
} from '../constants/plugin.action-types';
import { HIDE_PANEL, SHOW_PANEL } from '../constants/panel.action-types';

const initialState = {
  url: '',
  changesId: '',
  enabled: false,
  expanded: true
};

export const pluginReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PAGE_URL: {
      return { ...state, url: action.payload };
    }
    case SHOW_PLUGIN: {
      return { ...state, enabled: true };
    }
    case HIDE_PLUGIN: {
      return { ...state, enabled: false };
    }
    case SHOW_PANEL: {
      return { ...state, expanded: true };
    }
    case HIDE_PANEL: {
      return { ...state, expanded: false };
    }
    default:
      return state;
  }
};
