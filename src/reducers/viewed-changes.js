import { MARK_AS_VIEWED } from '../constants/viewed-changes.action-types';
import { getStoredVisitedValues } from '../actions/viewed-changes.actions';

const initialState = getStoredVisitedValues();

export const viewedChangesReducer = (state = initialState, action) => {
  switch (action.type) {
    case MARK_AS_VIEWED: {
      return { ...state, [action.payload]: true };
    }
    default:
      return state;
  }
};
